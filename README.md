# Website Performance Optimization portfolio project
The objective of this project was to optimize a portfolio page and a fictional pizza ordering page.

##How to start
Clone or download the udacity-nanodegree-proj04 and then inside the dist folder, you will find the compressed versions of all files.
Open the index.html to see Cam's portfolio, then click on the "Cam's Pizzeria" link to view the pizzeria page updates. 

### Changes made
* Used gulp to minify css,js, and HTML.

#### Cam Portfolio
* Compressed images
* Set print css to only show on print
* Removed render blocking css
* Added above the fold critical css onto the page and creatd a callback function to handle the rest of the css after the page loaded. 
* Move google analytics code to bottom of the page since it is not critical to showing the page to the user
* Added missing closing div tag
* Minify CSS
* Minify JS
* Minified HTML

#### Pizza
* Inlined CSS
* Minified CSS
* Compressed HTML
* Compressed JS
* Reduce total number of pizzas being generated to more of what is dispayed on screen.
* Moved DOM calling variables out side for loops so as to only make one DOM call.
* Changed the calculation of the sine function. Since there are 5 sines that were repeated each time. I calculated those 5 sines per scroll and then used them in the creation of each pizza mover.
* Used TranslateX for calculations instead of Javascripts left.
* With TranslateX, changed the initialization style of the element
* Used animation frame to handle update positions
